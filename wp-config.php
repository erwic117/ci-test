<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/** method WP uses to wirte to filesystem */

define ('FS_METHOD', 'direct');

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'O5zbE4_BxWGP~_;sfD`X(+s V>LT:$/!,Ma/D$&MIYBwQ9>/`Se1M-)_fo|A P0 ' );
define( 'SECURE_AUTH_KEY',  '#{eu=P[;%@y[6l?(@ESc$dT]ceO8ljH--wj`.3aULs}gZm^pINyP1[PRaMj/;(9-' );
define( 'LOGGED_IN_KEY',    'E6y$-&W:-_Ogk`5mBko32+6(5vi4~2R_dOr<1p8$qS=qJJqZiIVOPR:!S?o<>(97' );
define( 'NONCE_KEY',        'R5 bdlpzaS^FV4NKF,E+$-1h3f^|(& fPi/I$CF`.^|:e!Jxfw)0uZ{!F-1F6k-j' );
define( 'AUTH_SALT',        '3+glus-#^:d_,I~~N8)! ?7(|@#FzCan(zV,Wl|XH.Ipf-1O6zIJ(6-Z$fp{3C~K' );
define( 'SECURE_AUTH_SALT', '*?GXuQGKzu-vQg}a{TxZ1f8))gVPituvFw4[Id(XnR+u AI-Uo*4@FV$!o1#}$FR' );
define( 'LOGGED_IN_SALT',   ';Zhfq&PigGUU}w|-,fDg3GzQ8x@o4W{p#`c&aUu)ST{]whz8M9QWm5~PNUA<th+z' );
define( 'NONCE_SALT',       '1?_UAy+Ie!!B*+d%K7A+;DhOVDya8g [jXahfr|YsM)B]h5auJw9*&TomO,RfWDY' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
